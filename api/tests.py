from django.test import TestCase
from django.urls import reverse, resolve
from mpesa.mpesa_views import deposit, statement

# Create your tests here.
class DepositTests(TestCase):
    def setUp(self):
        pass
        #self.deposit_response = self.client.get(reverse('deposit'))

    def test_deposit_page_resolves_deposit_view(self):
        self.assertEquals(resolve(reverse('deposit')).func, deposit)

    #def test_deposit_view_status_code(self):
        #self.assertEquals(self.deposit_response.status_code, 200)


class StatementTests(TestCase):
    def setUp(self):
        self.statement_response = self.client.get(reverse('statement'))

    def test_statement_page_resolves_deposit_view(self):
        self.assertEquals(resolve(reverse('statement')).func, statement)

    def test_statement_view_status_code(self):
        self.assertEquals(self.statement_response.status_code, 200)

    def test_signup_page_contains_link_to_tip_page_deposit_page_statement_page(self):
        self.assertContains(self.statement_response, 'href="{0}"'.format(reverse('home')))
        self.assertContains(self.statement_response, 'href="{0}"'.format(reverse('deposit')))
        self.assertContains(self.statement_response, 'href="{0}"'.format(reverse('statement')))
