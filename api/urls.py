from django.urls import path
from api import views as mpesa_views

urlpatterns = [
    path('statement/', mpesa_views.statement, name='statement'),
    path('deposit/', mpesa_views.deposit, name='deposit'),
    path('online/lipa', mpesa_views.lipa_na_mpesa_online, name='lipa_na_mpesa'),

    # register, confirmation, validation and callback urls
    path('c2b/register', mpesa_views.register_urls, name="register_mpesa_validation"),
    path('c2b/validation', mpesa_views.validation, name='validation'),
    path('c2b/confirmation', mpesa_views.confirmation, name='confirmation'),
    path('c2b/callback', mpesa_views.call_back, name="call_back"),
    path('c2b/simulate', mpesa_views.simulate_transaction, name="simulate")
]
