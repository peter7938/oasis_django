import logging
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from requests.auth import HTTPBasicAuth
import json
import requests
from .credentials import MpesaC2bCredential, LipanaMpesaPassword
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from .models import ConfirmationUrl

logger = logging.getLogger(__name__)

@login_required
def deposit(request):
    return render(request, 'deposit.html')

@login_required
def statement(request):
    return render(request, 'statement.html')

#@cache_page(3000)
def get_access_token():
    r = requests.get(MpesaC2bCredential.api_URL,
                     auth=HTTPBasicAuth(MpesaC2bCredential.consumer_key, MpesaC2bCredential.consumer_secret))
    mpesa_access_token = r.json()
    return mpesa_access_token['access_token']


def lipa_na_mpesa_online(request):
    access_token = get_access_token()
    api_url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
    headers = {"Authorization": "Bearer %s" % access_token}
    request = {
        "BusinessShortCode": LipanaMpesaPassword.Business_short_code,
        "Password": LipanaMpesaPassword.decode_password,
        "Timestamp": LipanaMpesaPassword.lipa_time,
        "TransactionType": "CustomerPayBillOnline",
        "Amount": 1,
        "PartyA": 254704391679,  # replace with your phone number to get stk push
        "PartyB": LipanaMpesaPassword.Business_short_code,
        "PhoneNumber": 254704391679,  # replace with your phone number to get stk push
        "CallBackURL": "https://sandbox.safaricom.co.ke/api/c2b/callback",
        "AccountReference": "Peter",
        "TransactionDesc": "Testing stk push"
    }
    response = requests.post(api_url, json=request, headers=headers)
    return HttpResponse(response.text)

@csrf_exempt
def register_urls(request):
    access_token = get_access_token()
    api_url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl"
    headers = {"Authorization": "Bearer %s" % access_token}
    options = {"ShortCode": MpesaC2bCredential.shortcode,
               "ResponseType": "Completed",
               "ConfirmationURL": "https://fdf145ce.ngrok.io/api/c2b/confirmation",
               "ValidationURL": "https://fdf145ce.ngrok.io/api/c2b/validation"}
    response = requests.post(api_url, json=options, headers=headers)
    return HttpResponse(response.text)

@csrf_exempt
def simulate_transaction(request):
    access_token = get_access_token()
    api_url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate"
    headers = {"Authorization": "Bearer %s" % access_token}
    request = { "ShortCode":MpesaC2bCredential.shortcode,
        "CommandID":"CustomerPayBillOnline",
        "Amount":2,
        "Msisdn":"254708374149",
        "BillRefNumber":"2222" }

    response = requests.post(api_url, json = request, headers=headers)

    return HttpResponse(response.text)


@csrf_exempt
def confirmation(request):
    mpesa_body = request.body.decode('utf-8')
    mpesa_payment = json.loads(mpesa_body)

    file = open('confirmation.json', 'a')
    file.write(json.dumps(mpesa_payment))
    file.close()

    payment = ConfirmationUrl(
        first_name=mpesa_payment['FirstName'],
        last_name=mpesa_payment['LastName'],
        middle_name=mpesa_payment['MiddleName'],
        trans_id=mpesa_payment['TransID'],
        phone_number=mpesa_payment['MSISDN'],
        trans_amount=mpesa_payment['TransAmount'],
        organization_balance=mpesa_payment['OrgAccountBalance'],
        type=mpesa_payment['TransactionType'],
    )
    payment.save()

    context = {
        "ResultCode": 0,
        "ResultDesc": "Accepted"
    }

    return JsonResponse(dict(context))

@csrf_exempt
def call_back(request):
    pass

@csrf_exempt
def validation(request):
    context = {
        "ResultCode": 1,
        "ResultDesc": "Accepted"
    }
    mpesa_body = request.body.decode('utf-8')
    mpesa_payment = json.loads(mpesa_body)

    file = open('v.json', 'a')
    file.write(json.dumps(mpesa_payment))
    file.close()

    return JsonResponse(dict(context))
