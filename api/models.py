from django.db import models

class MpesaCall(models.Model):
    id = models.AutoField(primary_key=True)
    merchant_request_id = models.TextField(max_length=20)
    checkout_request_id = models.TextField(max_length=20)
    result_code = models.FloatField()
    result_desc = models.CharField(max_length=20)

    class Meta:
        verbose_name = 'MpesaCall'
        verbose_name_plural = 'MpesaCalls'

class MpesaCallBacks(models.Model):
    id = models.AutoField(primary_key=True)
    merchant_request_id = models.TextField(max_length=20)
    checkout_request_id = models.TextField(max_length=20)
    result_code = models.FloatField()
    result_desc = models.CharField(max_length=20)
    amount = models.FloatField()
    receipt_no = models.TextField(max_length=20)
    transaction_date = models.DateTimeField(auto_now_add=True)
    payer_phone = models.CharField(max_length=20)

    class Meta:
        verbose_name = 'MpesaCallBack'
        verbose_name_plural = 'MpesaCallBacks'


class ConfirmationUrl(models.Model):
    id = models.AutoField(primary_key=True)
    trans_id = models.CharField(max_length=20)
    type = models.TextField()
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    trans_amount = models.FloatField()
    phone_number = models.TextField()
    organization_balance = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = 'ConfirmationUrl'
        verbose_name_plural = 'ConfirmationUrl'

    def __str__(self):
        return self.phone_number

class Subscription(models.Model):
    id = models.AutoField(primary_key=True)
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField()

    class Meta:
        verbose_name = 'Subscription'
        verbose_name_plural = 'Subscriptions'
