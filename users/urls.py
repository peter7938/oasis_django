from django.urls import path
from users import views as users_views
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('register/', users_views.register, name='register'),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('verification/', users_views.verification, name='verification'),
    path('forgot_password/', users_views.forgot_password, name='forgot_password')
]
