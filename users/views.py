from django.shortcuts import render, redirect
from django.urls import reverse
#from .forms import UserProfileForm
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required


def register(request):

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            #added_user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Account created for {}!'.format(username))
            return redirect('login')

    else:
        form = UserCreationForm()

    context = {'form' : form}

    return render(request, 'signup.html', context)

def login(request):
    form = UserCreationForm()
    context = {'form' : form}
    return render(request, 'login.html', context)

def verification(request):
    return render(request, 'verify.html')

def forgot_password(request):
    return render(request, 'forgot_password.html')
