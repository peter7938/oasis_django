from django.test import TestCase
from django.urls import reverse, resolve
from users.users_views import register, login, verification, forgot_password
from .models import UserProfile

# Create your tests here.
class RegisterTests(TestCase):

    def setUp(self):
        self.home_url = reverse('home')
        self.deposit_url = reverse('deposit')
        self.statement_url = reverse('statement')
        self.register_response = self.client.get(reverse('register'))

    def test_csrf(self):
        self.assertContains(self.register_response, 'csrfmiddlewaretoken')

    def test_register_url_resolves_register_view(self):
        self.assertEquals(resolve(reverse('register')).func, register)

    def test_register_view_success_status_code(self):
        self.assertEquals(self.register_response.status_code, 200)

    def test_register_valid_post_data(self):
        data = {
            'phone':'0719413656',
            'password':'4393Gituru'
        }
        response = self.client.post(reverse('register'), data)
        self.assertTrue(UserProfile.objects.exists())

    def test_register_not_valid_post_data(self):
         response = self.client.post(reverse('register'), {})
         self.assertEquals(self.register_response.status_code, 200)

    def test_new_topic_invalid_post_data_empty_fields(self):
        '''
        Invalid post data should not redirect
        The expected behavior is to show the form again with validation errors
        '''
        data = {
            'phone': '',
            'password': ''
        }
        response = self.client.post(reverse('register'), data)
        self.assertEquals(self.register_response.status_code, 200)
        self.assertFalse(UserProfile.objects.exists())

    def test_signup_page_contains_link_to_tip_page_deposit_page_statement_page(self):
        self.assertContains(self.register_response, 'href="{0}"'.format(reverse('home')))
        self.assertContains(self.register_response, 'href="{0}"'.format(reverse('deposit')))
        self.assertContains(self.register_response, 'href="{0}"'.format(reverse('statement')))
        self.assertContains(self.register_response, 'href="{0}"'.format(reverse('login')))
        self.assertContains(self.register_response, 'href="{0}"'.format(reverse('register')))

class LoginTests(TestCase):
    def setUp(self):
        self.login_response = self.client.get(reverse('login'))

    def test_login_url_resolves_login_view(self):
        self.assertEquals(resolve(reverse('login')).func, login)

    def test_login_view_success_status_code(self):
        self.assertEquals(self.login_response.status_code, 200)

    def test_signup_page_contains_link_to_tip_page_deposit_page_statement_page(self):
        self.assertContains(self.login_response, 'href="{0}"'.format(reverse('home')))
        self.assertContains(self.login_response, 'href="{0}"'.format(reverse('deposit')))
        self.assertContains(self.login_response, 'href="{0}"'.format(reverse('statement')))
        self.assertContains(self.login_response, 'href="{0}"'.format(reverse('login')))
        self.assertContains(self.login_response, 'href="{0}"'.format(reverse('register')))

class VerificationTests(TestCase):
    def setUp(self):
        self.verification_response = self.client.get(reverse('verification'))

    def test_verification_url_resolves_verification_view(self):
        self.assertEquals(resolve(reverse('verification')).func, verification)

    def test_verification_view_success_status_code(self):
        self.assertEquals(self.verification_response.status_code, 200)

    def test_verification_page_contains_link_to_tip_page_deposit_page_statement_page(self):
         self.assertContains(self.verification_response, 'href="{0}"'.format(reverse('home')))
         self.assertContains(self.verification_response, 'href="{0}"'.format(reverse('deposit')))
         self.assertContains(self.verification_response, 'href="{0}"'.format(reverse('statement')))
         self.assertContains(self.verification_response, 'href="{0}"'.format(reverse('login')))
         self.assertContains(self.verification_response, 'href="{0}"'.format(reverse('register')))

class Forgot_password(TestCase):
    def setUp(self):
        self.forgot_password_response = self.client.get(reverse('forgot_password'))

    def test_forgot_password_url_resolves_forgot_view(self):
        self.assertEquals(resolve(reverse('forgot_password')).func, forgot_password)

    def test_forgot_password_view_status_code(self):
        self.assertEquals(self.forgot_password_response.status_code, 200)

    def test_verification_page_contains_link_to_tip_page_deposit_page_statement_page(self):
         self.assertContains(self.forgot_password_response, 'href="{0}"'.format(reverse('home')))
         self.assertContains(self.forgot_password_response, 'href="{0}"'.format(reverse('deposit')))
         self.assertContains(self.forgot_password_response, 'href="{0}"'.format(reverse('statement')))
         self.assertContains(self.forgot_password_response, 'href="{0}"'.format(reverse('login')))
         self.assertContains(self.forgot_password_response, 'href="{0}"'.format(reverse('register')))
