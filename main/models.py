from django.db import models

# Create your models here.
class League(models.Model):
    name = models.CharField(max_length=20, unique=True)

    class Meta:
        verbose_name = 'League'
        verbose_name_plural = 'League'

    def __str__(self):
        return self.name

class Team(models.Model):
    name = models.CharField(max_length=20)
    related_league = models.ForeignKey(League, related_name='team', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Team'
        verbose_name_plural = 'Teams'

    def __str__(self):
        return self.name

class Tips(models.Model):
    prediction_choices = (
        ('Home', 'Home'),
        ('Draw', 'Draw'),
        ('Away', 'Away'),
    )

    league = models.ForeignKey(League, related_name='league', on_delete=models.CASCADE)
    home_team = models.ForeignKey(Team, related_name='home_team', on_delete=models.CASCADE)
    away_team = models.ForeignKey(Team, related_name='away_team', on_delete=models.CASCADE)
    odd = models.DecimalField(decimal_places=2, max_digits=3)
    prediction = models.CharField(max_length=20, choices=prediction_choices)
    gamedate = models.DateField()
    date_today = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Tips'
        verbose_name_plural = 'Tips'
