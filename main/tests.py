from django.test import TestCase
from django.urls import reverse, resolve
from .main_views import home, tip
#from mpesa.mpesa_views import deposit, statement

# Create your tests here.
class HomeTests(TestCase):
    def setUp(self):
        self.home_url = reverse('home')
        self.tip_url = reverse('tip')
        self.deposit_url = reverse('deposit')
        self.statement_url = reverse('statement')
        self.login_url = reverse('login')
        self.register_url = reverse('register')
        self.home_response = self.client.get(self.home_url)

    def test_home_url_resolves_home_view(self):
        self.assertEquals(resolve(self.home_url).func, home)

    def test_home_view_status_code(self):
        self.assertEquals(self.home_response.status_code, 200)

    def test_home_page_contains_link_to_tip_page_deposit_page_statement_page(self):
        self.assertContains(self.home_response, 'href="{0}"'.format(self.tip_url))
        self.assertContains(self.home_response, 'href="{0}"'.format(self.deposit_url))
        self.assertContains(self.home_response, 'href="{0}"'.format(self.statement_url))
        self.assertContains(self.home_response, 'href="{0}"'.format(self.login_url))
        self.assertContains(self.home_response, 'href="{0}"'.format(self.register_url))

class TipTests(TestCase):
    def setUp(self):
        self.home_url = reverse('home')
        self.tip_url = reverse('tip')
        self.deposit_url = reverse('deposit')
        self.statement_url = reverse('statement')
        self.tip_response = self.client.get(self.tip_url)

    def test_tip_url_resolves_tip_view(self):
        self.assertEquals(resolve(self.tip_url).func, tip)

    def test_tip_view_status_code(self):
        self.assertEquals(self.tip_response.status_code, 200)

    def test_tip_page_contains_link_back_to_home_page_deposit_page_statement_page(self):
        self.assertContains(self.tip_response, 'href="{0}"'.format(self.home_url))
        self.assertContains(self.tip_response, 'href="{0}"'.format(self.deposit_url))
        self.assertContains(self.tip_response, 'href="{0}"'.format(self.statement_url))
