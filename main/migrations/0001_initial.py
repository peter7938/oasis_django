# Generated by Django 2.2 on 2020-05-23 06:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='League',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, unique=True)),
            ],
            options={
                'verbose_name': 'League',
                'verbose_name_plural': 'League',
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('related_league', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='team', to='main.League')),
            ],
            options={
                'verbose_name': 'Team',
                'verbose_name_plural': 'Teams',
            },
        ),
        migrations.CreateModel(
            name='Tips',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('odd', models.DecimalField(decimal_places=2, max_digits=3)),
                ('prediction', models.CharField(choices=[('Home', 'Home'), ('Draw', 'Draw'), ('Away', 'Away')], max_length=20)),
                ('gamedate', models.DateField()),
                ('date_today', models.DateTimeField(auto_now=True)),
                ('away_team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='away_team', to='main.Team')),
                ('home_team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='home_team', to='main.Team')),
                ('league', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='league', to='main.League')),
            ],
            options={
                'verbose_name': 'Tips',
                'verbose_name_plural': 'Tips',
            },
        ),
    ]
